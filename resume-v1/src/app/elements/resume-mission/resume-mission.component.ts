import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { ResumeViewDataService } from '../../services/resume-view-data.service';


@Component({
  selector: 'ws-resume-mission',
  templateUrl: './resume-mission.component.html',
  styleUrls: ['./resume-mission.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ResumeMissionComponent implements OnInit {

  constructor(private viewDataService: ResumeViewDataService) {
  }

  ngOnInit() {
  }

}
