import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumeExpertiseComponent } from './resume-expertise.component';

describe('ResumeExpertiseComponent', () => {
  let component: ResumeExpertiseComponent;
  let fixture: ComponentFixture<ResumeExpertiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumeExpertiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumeExpertiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
