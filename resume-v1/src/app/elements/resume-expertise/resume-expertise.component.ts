import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { ResumeViewDataService } from '../../services/resume-view-data.service';


@Component({
  selector: 'ws-resume-expertise',
  templateUrl: './resume-expertise.component.html',
  styleUrls: ['./resume-expertise.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ResumeExpertiseComponent implements OnInit {

  constructor(private viewDataService: ResumeViewDataService) {
  }

  ngOnInit() {
  }

}
