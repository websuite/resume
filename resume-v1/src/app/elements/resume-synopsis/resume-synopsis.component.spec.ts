import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumeSynopsisComponent } from './resume-synopsis.component';

describe('ResumeSynopsisComponent', () => {
  let component: ResumeSynopsisComponent;
  let fixture: ComponentFixture<ResumeSynopsisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumeSynopsisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumeSynopsisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
