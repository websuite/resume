import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { ResumeViewDataService } from '../../services/resume-view-data.service';


@Component({
  selector: 'ws-resume-summary',
  templateUrl: './resume-summary.component.html',
  styleUrls: ['./resume-summary.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ResumeSummaryComponent implements OnInit {

  constructor(private viewDataService: ResumeViewDataService) {
  }

  ngOnInit() {
  }

}
