import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';

import { MaterialComponentsModule } from './material-components.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ResumePageComponent } from './pages/resume-page/resume-page.component';
import { ResumeExperienceComponent } from './elements/resume-experience/resume-experience.component';
import { ResumeEducationComponent } from './elements/resume-education/resume-education.component';
import { ResumeMissionComponent } from './elements/resume-mission/resume-mission.component';
import { ResumeSummaryComponent } from './elements/resume-summary/resume-summary.component';
import { ResumeSynopsisComponent } from './elements/resume-synopsis/resume-synopsis.component';
import { ResumeExpertiseComponent } from './elements/resume-expertise/resume-expertise.component';


@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    ResumePageComponent,
    ResumeExperienceComponent,
    ResumeEducationComponent,
    ResumeMissionComponent,
    ResumeSummaryComponent,
    ResumeSynopsisComponent,
    ResumeExpertiseComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    MaterialComponentsModule,
    AppRoutingModule
  ],
  providers: [
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
