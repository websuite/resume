import { TestBed, inject } from '@angular/core/testing';

import { ResumeViewDataService } from './resume-view-data.service';

describe('ResumeViewDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResumeViewDataService]
    });
  });

  it('should be created', inject([ResumeViewDataService], (service: ResumeViewDataService) => {
    expect(service).toBeTruthy();
  }));
});
